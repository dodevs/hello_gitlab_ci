# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :hello_gitlab_ci,
  ecto_repos: [HelloGitlabCi.Repo]

# Configures the endpoint
config :hello_gitlab_ci, HelloGitlabCi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mauT3Je2xeEMJQAgYQS0zfBVG5D1pk5wLD9lOsWXuJTyU8cixYVWwy+cfGuGDVyy",
  render_errors: [view: HelloGitlabCi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: HelloGitlabCi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
